#/bin/bash
echo "source envsetup"
. build/envsetup.sh >/dev/null
echo "lunch"
lunch aosp_hammerhead-eng >/dev/null
echo "mmm"
mmm frameworks/base/policy/ >/dev/null
echo "adb root adb remount"
adb root >/dev/null
adb remount >/dev/null
echo "push"
adb push out/target/product/hammerhead/system/framework/android.policy.jar /system/framework
echo "reboot"
adb reboot
